import { History } from 'history';
import { match } from 'react-router-dom';
import Api from '../api';
import CampaignsRoute from './Campaigns';
import CampaignRoute from './Campaign';
import CampaignCreate from './CampaignCreate';
import { RouteProps } from '../routes';
import HomeRoute from './Home';

export interface ICampaignsRoute {
  path: RoutePath | string[];
  component: React.ComponentClass<RouteProps<any>>;
  exact: boolean;
}

export type RoutePath =
  | '/'
  | '/campaigns/:campaignId'
  | '/campaigns'
  | '/campaigns/new';

const routes: ICampaignsRoute[] = [
  {
    path: '/',
    component: HomeRoute,
    exact: true
  },
  {
    path: '/campaigns',
    component: CampaignsRoute,
    exact: true
  },
  {
    path: '/campaigns/new',
    component: CampaignCreate,
    exact: true
  },
  {
    path: '/campaigns/:campaignId',
    component: CampaignRoute,
    exact: true
  }
];

export interface RouteProps<Params = {}> {
  match: match<Params>;
  location: Location;
  api: Api;
  history: History;
}

export default routes;
