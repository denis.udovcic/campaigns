import React from 'react';
import Campaigns from '../../components/Campaigns';
import { RouteProps } from '..';

interface Params {}

interface Props extends RouteProps<Params> {}

interface State {
  campaigns?: api.models.ICampaign[];
}

export default class CampaignsRoute extends React.Component<Props, State> {
  state: State = {};
  componentDidMount = async () => {
    this.setState({
      campaigns: await this.props.api.getCampaigns()
    });
  };
  render = () => {
    if (!this.state.campaigns) {
      return <p>no campaings</p>;
    }
    return (
      this.state.campaigns && (
        <Campaigns
          campaigns={this.state.campaigns}
          onCampaignClick={this.navigateToCampaign}
        />
      )
    );
  };

  private navigateToCampaign = (id: string) => {
    this.props.history.push(`/campaigns/${id}`);
  };
}
