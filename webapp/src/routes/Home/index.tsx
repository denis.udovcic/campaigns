import React from 'react';
import { Redirect } from 'react-router';

export default class CampaignsRoute extends React.Component<any> {
  render() {
    return <Redirect to="/campaigns" />;
  }
}
