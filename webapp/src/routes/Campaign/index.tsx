import React from 'react';
import Campaign from '../../components/Campaign';
import { RouteProps } from '..';

interface Params {
  campaignId: string;
}

interface Props extends RouteProps<Params> {}

interface State {
  campaign?: api.models.ICampaign;
}

class CampaignRoute extends React.Component<Props, State> {
  state: State = {};

  componentDidMount = async () => {
    const { campaignId } = this.props.match.params;
    this.setState({
      campaign: await this.props.api.getCampaign(campaignId)
    });
  };
  render() {
    if (!this.state.campaign) {
      return <p>no campaign</p>;
    }
    return (
      <div>
        <Campaign campaign={this.state.campaign} />
      </div>
    );
  }
}

export default CampaignRoute;
