import React from 'react';
import CampaignCreate from '../../components/CampaignCreate';
import { RouteProps } from '..';

interface Params {}

interface Props extends RouteProps<Params> {}

class CampaignRoute extends React.Component<Props, never> {
  render() {
    return (
      <CampaignCreate
        onCreate={c => this.props.api.createCampaign(c)}
      />
    );
  }
}

export default CampaignRoute;
