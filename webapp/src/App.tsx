import React, { Fragment } from 'react';
import './App.css';
import { Route, Switch } from 'react-router-dom';
import Api from './api';
import { ICampaignsRoute } from './routes';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { createMuiTheme } from '@material-ui/core/styles';
import indigo from '@material-ui/core/colors/indigo';
import pink from '@material-ui/core/colors/pink';
import red from '@material-ui/core/colors/red';
import Navigation from './components/common/Navigation';

const theme = createMuiTheme({
  palette: {
    primary: indigo,
    secondary: pink,
    error: red,
    contrastThreshold: 3,
    tonalOffset: 0.2
  }
});

interface Props {
  api: Api;
  routes: ICampaignsRoute[];
}

interface State {
  campaigns?: api.models.ICampaign[];
}

class App extends React.Component<Props, State> {
  state: State = {
    campaigns: undefined
  };
  render() {
    return (
      <div className="App">
        <MuiThemeProvider theme={theme}>
          <Switch>
            {this.props.routes.map((r: ICampaignsRoute, i: number) => {
              return (
                <Route
                  key={i}
                  exact={r.exact}
                  path={r.path}
                  render={(props: any) => (
                    <Fragment>
                      <Navigation
                        onRouteChange={(route: string) => {
                          props.history.push(route);
                        }}
                        route={r.path}
                        {...props}
                      />
                      <r.component {...props} api={this.props.api} />
                    </Fragment>
                  )}
                />
              );
            })}
          </Switch>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default App;
