import React from 'react';

interface Props {
  campaign: api.models.ICampaign;
}

export default class Campaign extends React.Component<Props> {
  render() {
    const { campaign } = this.props;
    return (
      <div className="Campaign">
        <p>Start: {campaign.startDate}</p>
        <p>End: {campaign.endDate}</p>
        <p>Target impressions: {campaign.targetImpressions}</p>
      </div>
    );
  }
}
