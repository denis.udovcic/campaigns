import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import { RouteProps, RoutePath } from '../../../routes';

const useStyles = makeStyles({
  root: {
    width: 500
  }
});

interface Props extends RouteProps<never> {
  route: RoutePath;
  onRouteChange(route: any): void;
}

export default (props: Props) => {
  const classes = useStyles();
  return (
    <BottomNavigation
      onChange={(_event, newValue) => {
        props.onRouteChange(newValue);
      }}
      showLabels={true}
      value={props.route}
      className={classes.root}
      style={{ display: 'flex', margin: 'auto' }}
    >
      <BottomNavigationAction
        label="Campaigns"
        value="/campaigns"
      />
      <BottomNavigationAction
        label="Create Campaign"
        value="/campaigns/new"
      />
    </BottomNavigation>
  );
};
