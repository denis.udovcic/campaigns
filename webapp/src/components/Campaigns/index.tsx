import React from 'react';
import moment from 'moment';
import './Campaigns.css';

interface Props {
  campaigns?: api.models.ICampaign[];
  onCampaignClick(id: string): void;
}

// put into utils
const timestampToTime = (time: moment.MomentInput) => {
  return moment(time).format('DD/MM/YYYY HH:mm:ss');
};

export default class Campaigns extends React.Component<Props> {
  render() {
    return this.props.campaigns ? (
      <div className="Campaigns">
        {this.props.campaigns.map(
          (
            { id, startDate, endDate, targetImpressions }: api.models.ICampaign,
            i: number
          ) => {
            return (
              <div
                className="campaign-container"
                onClick={() => this.props.onCampaignClick(id)}
                key={i}
              >
                <p>Id: {id}</p>
                <p>Start: {timestampToTime(startDate)}</p>
                <p>End: {timestampToTime(endDate)}</p>
                <p>Target impressions: {targetImpressions}</p>
              </div>
            );
          }
        )}
      </div>
    ) : (
      <p>asdawdaw</p>
    );
  }
}
