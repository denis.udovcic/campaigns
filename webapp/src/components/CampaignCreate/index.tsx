import React from 'react';
import './CampaignCreate.css';
import { TextField, IconButton } from '@material-ui/core';

interface Props {
  onCreate(c: api.models.ICampaignCreate): void;
}

interface State {
  startDate?: string;
  endDate?: string;
  targetImpressions?: number;
}

export default class CampaignCreate extends React.Component<Props, State> {
  state: State = {
    startDate: '2017-05-24T10:30',
    endDate: '2020-05-24T10:30',
    targetImpressions: 0,

  };
  render() {
    return (
      <div className="CampaignCreate">
        <TextField
          value={this.state.startDate}
          onChange={this.onUpdate('startDate')}
          label="Next appointment"
          type="datetime-local"
          InputLabelProps={{
            shrink: true
          }}
        />
        <TextField
          value={this.state.endDate}
          onChange={this.onUpdate('endDate')}
          label="Next appointment"
          type="datetime-local"
          InputLabelProps={{
            shrink: true
          }}
        />
        <TextField
          value={this.state.targetImpressions}
          onChange={this.onUpdate('targetImpressions')}
          type="number"
        />
        <IconButton onClick={() => this.props.onCreate(this.state as any)}>
          Create
        </IconButton>
      </div>
    );
  }

  private onUpdate = (p: keyof State) => (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    this.setState({
      ...this.state,
      [p]: e.target.value
    });
  };
}
