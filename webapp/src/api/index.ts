import { HttpClient } from './http-client';
import { AxiosError, AxiosResponse } from 'axios';
import { config } from '../config';

export default class Api extends HttpClient {
  constructor() {
    super({
      baseURL: config.apiBaseUrl,
      onError: (apiError: AxiosError) => {
        console.error('failed to send request to api', apiError);
        throw apiError;
      },
      responseDataMapper(res: AxiosResponse<any>) {
        return res.data;
      }
    });
  }

  public getCampaigns = (): Promise<api.models.ICampaign[]> =>
    this.get<api.models.ICampaign[]>('/campaigns/all');

  public getCampaign = (id: string): Promise<api.models.ICampaign> =>
    this.get<api.models.ICampaign>(`/campaigns/${id}`);

  public createCampaign = (body: api.models.ICampaignCreate) =>
    this.post<void>('/campaigns', body);
}
