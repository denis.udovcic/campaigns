import Axios, { AxiosInstance, AxiosResponse } from 'axios';
import * as uuid from 'uuid';
import * as qs from 'querystring';

export class AdServerService {
  private axiosClient: AxiosInstance;

  constructor(config: config.IConfig) {
    this.axiosClient = Axios.create({
      baseURL: config.adServerUrl,
      headers: {
        'Content-Type': 'application/json',
        'X-API-Key': config.apiKey
      }
    });
  }

  public getAllCampaigns = async (): Promise<api.models.ICampaign[]> => {
    const result = await this.axiosClient.get<api.models.ICampaign[]>(
      '/campaigns/*'
    );

    return result && result.data;
  };

  public getCampaign = async (
    campaignId: string
  ): Promise<api.models.ICampaign> => {
    const campaign = await this.axiosClient.get<api.models.ICampaign>(
      `/campaigns/${campaignId}`
    );

    return campaign && campaign.data;
  };

  public createCampaign = async (
    campaign: api.models.ICampaignCreate
  ): Promise<string> => {
    const data = qs.stringify({ id: uuid.v4(), ...campaign });
    const created = await this.axiosClient.post<
      api.models.ICampaignCreate,
      AxiosResponse<string>
    >('/campaigns', data, {
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    });

    return created && created.data;
  };
}
