import { AdServerService } from './AdServerService';

export interface IServices {
  adServer: AdServerService;
}

export const createServices = (config: config.IConfig): IServices => {
  return {
    adServer: new AdServerService(config)
  };
};
