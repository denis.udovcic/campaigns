declare namespace config {
    interface IConfig {
        port: number;
        adServerUrl: string;
        apiKey: string;
    }
}