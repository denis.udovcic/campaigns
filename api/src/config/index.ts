const config: config.IConfig = {
    port: 4000,
    apiKey: process.env['X_API_KEY']!, // '!' could not be there
    adServerUrl: 'https://esobbc6302.execute-api.eu-west-1.amazonaws.com/default'
}

export default config;