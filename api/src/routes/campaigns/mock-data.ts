export const campaigns: any[] = [
  {
    id: 'a5b3e3a0-a4b0-11e9-9e26-958d5defb7c5',
    startDate: 1562941420320,
    endDate: 1562945020320,
    targetImpressions: 42115,
    deliveredImpressions: 42058
  },
  {
    id: 'fe8b24a0-a4ad-11e9-9e26-958d5defb7c5',
    startDate: 1562940281079,
    endDate: 1562943881080,
    targetImpressions: 37902,
    deliveredImpressions: 37842
  },
  {
    id: 'aaef4e40-a4b0-11e9-9e26-958d5defb7c5',
    startDate: 1562941428155,
    endDate: 1562945028155,
    targetImpressions: 12688,
    deliveredImpressions: 12643
  },
  {
    id: 'b85fb6a0-a4bf-11e9-9e26-958d5defb7c5',
    startDate: 1562947893941,
    endDate: 1562951493942,
    targetImpressions: 31794,
    deliveredImpressions: 31276
  },
  {
    id: '11319e40-a4ae-11e9-9e26-958d5defb7c5',
    startDate: 1562940311799,
    endDate: 1562943911800,
    targetImpressions: 17447,
    deliveredImpressions: 17271
  },
  {
    id: '042020f0-a4ae-11e9-9e26-958d5defb7c5',
    startDate: 1562940289331,
    endDate: 1562943889331,
    targetImpressions: 33793,
    deliveredImpressions: 33662
  }
];
