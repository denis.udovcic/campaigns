import { Router } from 'express';
import CampaignsController from './campaigns.controller';
import { UnauthorisedRouteHandlerBuilder } from '../utils/route-utils';
import { IServices } from '../../services';

const campaignController = (
  c: config.IConfig,
  s: IServices
): CampaignsController => new CampaignsController(c, s);

const campaignsRouter = Router();

export const routes = (
  unauthRequest: UnauthorisedRouteHandlerBuilder,
): Router => {
  campaignsRouter.get(
    '/all',
    unauthRequest<
      void,
      void,
      void,
      api.models.ICampaign[],
      CampaignsController
    >(campaignController, (c: CampaignsController) => c.getCampaigns)
  );
  campaignsRouter.get(
    '/:id',
    unauthRequest<
      { id: string },
      void,
      void,
      api.models.ICampaign,
      CampaignsController
    >(campaignController, (c: CampaignsController) => c.getCampaign)
  );
  campaignsRouter.post(
    '/',
    unauthRequest<
      void,
      api.models.ICampaignCreate,
      void,
      string,
      CampaignsController
    >(campaignController, (c: CampaignsController) => c.createCampaign)
  );
  return campaignsRouter;
};
