import { UnauthTypedRequest } from '../../middleware/request-middleware';
import { IServices } from '../..//services';
import { validateCampaignCreate } from '../utils/campaign-utils';
import { dateToUTCEpoch } from '../utils';
import { notFound, badRequest, internal } from '../../errors/error-response';

export default class CampaignsController {
  constructor(
    protected config: config.IConfig,
    protected services: IServices
  ) {}

  public getCampaigns = async (
    _req: UnauthTypedRequest<void, void, void>
  ): IApiResponse<api.models.ICampaign[]> => {
    const results = await this.services.adServer.getAllCampaigns();

    if (results) {
      return results;
    }

    return notFound('No results');
  };

  public getCampaign = async (
    req: UnauthTypedRequest<{ id: string }, void, void>
  ): IApiResponse<api.models.ICampaign> => {
    const campaignId = req.params.id;

    if (!campaignId) {
      return null as any;
    }
    const results = await this.services.adServer.getCampaign(campaignId);

    if (results) {
      return results;
    }

    return notFound('Could not find the campaign!');
  };

  public createCampaign = async (
    req: UnauthTypedRequest<void, api.models.ICampaignCreate, void>
  ): IApiResponse<string> => {
    const campaign = req.body;

    const validationErrors = validateCampaignCreate(campaign);
    if (validationErrors.length) {
      return badRequest('ValidationError', validationErrors);
    }

    const data: api.models.ICampaignCreate = {
      startDate: dateToUTCEpoch(campaign.startDate),
      endDate: dateToUTCEpoch(campaign.endDate),
      targetImpressions: Number(campaign.targetImpressions)
    };
    const results = await this.services.adServer.createCampaign(data);

    if (results) {
      return results;
    }

    return internal('Something went wrong');
  };
}
