import * as moment from 'moment';

const isValidTime = (times: any[]) =>
  times.every((time: any) => moment(time).isValid());

export const validateCampaignCreate = (
  campaign?: api.models.ICampaignCreate
): string[] => {
  const errors: string[] = [];
  if (!campaign) {
    errors.push('Campaign not found');
    return errors;
  }
  const { startDate, endDate, targetImpressions } = campaign;

  if (!startDate || !endDate || !targetImpressions) {
    errors.push(
      'Start date, End date and Target impressions must be provided!'
    );
  }

  if (isNaN(Number(targetImpressions))) {
    errors.push('Target impressions not in valid format!');
  }

  if (!isValidTime([startDate, endDate])) {
    errors.push('Start and End date must be in valid time format!');
  }

  if (moment(startDate).isAfter(endDate)) {
    errors.push('Start date must be before end date!');
  }
  return errors;
};
