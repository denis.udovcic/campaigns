import { Application } from 'express';
import * as campaigns from './campaigns';
import { IServices } from '../services';
import { unauthRequestHandlerBuilder } from './utils/route-utils';

const routesConfig = (
  app: Application,
  config: config.IConfig,
  services: IServices
): void => {
  const unauthRequest = unauthRequestHandlerBuilder(config, services);
  app.use('/campaigns', campaigns.routes(unauthRequest));
};

export default routesConfig;
