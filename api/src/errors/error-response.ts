export const notFound = (reason: string): api.models.IErrorResponse => ({
  statusCode: 404,
  reason
});

export const badRequest = (reason: string, validationErrors: string[]): api.models.IErrorResponse => ({
  statusCode: 400,
  reason,
  validationErrors
});

export const internal = (reason: string): api.models.IErrorResponse => ({
  statusCode: 500,
  reason
});
