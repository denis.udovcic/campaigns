// 'Cannot find Omit' workaround
type Without<T, K extends keyof any> = Pick<T, Exclude<keyof T, K>>;
declare module api.models {
  interface ICampaign {
    id: string;
    startDate: number;
    endDate: number;
    targetImpressions: number;
  }
  type ICampaignCreate = Without<ICampaign, 'id'>;
}
