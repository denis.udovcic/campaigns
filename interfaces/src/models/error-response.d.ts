declare namespace api.models {
    interface IErrorResponse {
      statusCode: number;
      reason: string;
      validationErrors?: string[];
    }
  }
  